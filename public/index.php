<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

require_once '../vendor/autoload.php';
require_once '../include/db_handler.php';
require_once '../include/upload.php';
require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
use duncan3dc\Laravel\Blade;

$app = new \Slim\Slim();

function asset($path)
{
    return '/'.$path;
}

$log_get = json_encode($_GET) . PHP_EOL;
$log_post = json_encode($_POST) . PHP_EOL;
$log_body = file_get_contents("php://input") . PHP_EOL;
$divider = '-----------------------------------------------------------'.PHP_EOL;
$blade = new \duncan3dc\Laravel\BladeInstance(__DIR__."/../views", __DIR__."/../cache/views");

file_put_contents('logs.txt', $divider.$log_get.$log_post.$log_body.$divider , FILE_APPEND | LOCK_EX);

include_once '../apps/api.php';
include_once '../apps/admin.php';

$app->run();
