ALTER TABLE users ADD role ENUM ('admin', 'member') DEFAULT 'member' NULL
INSERT INTO `users` (`username`, `name`, `email`, `password`, `api`, `role`) VALUES
                    ('admin', 'administrator', 'admin@mail.com',
                     '$2y$10$pXnG2f.4qftuQE8yC4IAZOCAlYsBDCsObCesFNiQ5.tZ65mpMwB0O',
                     'zzzzzz', 'admin');