<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <title>Backplan - Login</title>
  <?php echo $__env->make('partials.assets.stylesheet', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body id="auth_wrapper" >
<div id="login_wrapper">
  <div id="login_content">
    <h1 class="login-title">
      Admin Login
    </h1>
    <div class="login-body">
      <form action="" method="post">
        <div class="form-group label-floating is-empty">
          <label class="control-label" for="username">Username</label>
          <input type="text" class="form-control" id="username" name="username">
        </div>
        <div class="form-group label-floating is-empty">
          <label class="control-label" for="password">Password</label>
          <input type="password" class="form-control" id="password" name="password">
        </div>
        <input type="submit" class="btn btn-info btn-block m-t-40" value="Masuk">
      </form>
    </div>
  </div>
</div>
<?php echo $__env->make('partials.assets.javascript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>
