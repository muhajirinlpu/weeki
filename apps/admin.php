<?php
/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 08/09/17 , 20:13
 */

session_start();

function auth() {
    if (empty($_SESSION['admin_data'])) {
        $app = \Slim\Slim::getInstance();
        $app->flash('message', 'login required');
        $app->redirect('/auth/login');
    }
}

function guest() {
    if (isset($_SESSION['admin_data'])) {
        $app = \Slim\Slim::getInstance();
        $app->flash('message', 'Already logged in');
        $app->redirect('/admin/dashboard');
    }
}

$app->get('/auth/login', 'guest', function () use ($app, $blade) {
    echo $blade->render('auth.login');
});

$app->post('/auth/login', 'guest', function () use ($app, $blade) {
    $username = $app->request->params('username');
    $password = $app->request->params('password');
    $db       = new DbHandler();

    $db->adminLogin($username, $password);
});

$app->group('/admin', 'auth', function () use ($app, $blade) {
    $app->post('/logout', function () use ($app) {
        unset($_SESSION['admin_data']);
        $app->redirect('auth/login');
    });

    $app->get('/', function () use ($app) {
        $app->redirect('admin/dashboard');
    });

    $app->get('/dashboard', function () use ($blade) {
        echo $blade->render('app.dashboard');
    });
});