<?php

class PassHash
{

    public static function unique_salt()
    {
        return substr(sha1(mt_rand()), 0, 22);
    }
    public static function hash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
    public static function check_password($hash, $password)
    {
        return password_verify($password, $hash);
    }

}
?>
