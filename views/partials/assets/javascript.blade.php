<script src="{{ asset('js/vendor.bundle.js') }}"></script>
<script src="{{ asset('js/app.bundle.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@stack('javascript')
