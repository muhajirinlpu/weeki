<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  @isset($pageMeta)
    @foreach($pageMeta as $name => $content)
      <meta name="{{ $name }}" content="{{ $content }}">
    @endforeach
  @endisset
  <title>Backendplan Admin &ndash; {{ $pageTitle or '' }}</title>
  @include('partials.assets.stylesheet')
  @stack('stylesheet')
</head>
<body>
<div id="app_wrapper">
  <!-- Top Menu Bar Wrapper -->
  <header id="app_topnavbar-wrapper">
    <nav role="navigation" class="navbar topnavbar">
      <div class="nav-wrapper">
        <ul class="nav navbar-nav pull-left left-menu">
          <li class="app_menu-open">
            <a href="javascript:void(0)" data-toggle-state="app_sidebar-left-open" data-key="leftSideBar">
              <i class="zmdi zmdi-menu"></i>
            </a>
          </li>
        </ul>
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown avatar-menu">
            <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
								<span class="meta">
									<span class="name">{{ $_SESSION['admin_data']['name'] }}</span>
									<span class="caret"></span>
								</span>
              <div class="ripple-container"></div></a>
            <ul class="dropdown-menu btn-primary dropdown-menu-right">
              <li>
                <a href="#"><i class="zmdi zmdi-settings"></i> Pengaturan Akun </a>
              </li>
              <li>
                <form action="/admin/logout" method="post"
                      class="hidden" id="logout-form">
                </form>
                <a href="javascript:void(0)" onclick="document.getElementById('logout-form').submit()">
                  <i class="zmdi zmdi-sign-in"></i> Keluar
                </a>
              </li>
            </ul>
          </li>
          <li class="last">
            <a href="javascript:void(0)" data-toggle-state="sidebar-overlay-open" data-key="rightSideBar">
              <i class="mdi mdi-playlist-plus"></i>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!--/ Top Menu Bar Wrapper -->

  {{-- Left Sidebar Wrapper --}}
  @include('partials.menu.left-sidebar')
  {{--/ Left Sidebar Wrapper --}}

  <!-- Main Content Wrapper -->
  <section id="content_outer_wrapper">
    <div id="content_wrapper">
      <div id="header_wrapper" class="header-sm">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12">
              <header id="header">
                <h1>{{ $pageTitle or 'Untitled' }}</h1>
              </header>
            </div>
          </div>
        </div>
      </div>
      <div id="content" class="container-fluid">
        <div class="content-body">
          @yield('content')
        </div>
      </div>
    </div>
    <!-- Footer Wrapper -->
    <footer id="footer_wrapper" style="width: 100%">
      <div class="footer-content">
        <div class="row copy-wrapper">
          <div class="col-xs-8">
            <p class="copy">&copy; Copyright <time class="year">2017</time> ACDSee - All Rights Reserved</p>
          </div>
        </div>
      </div>
    </footer>
    <!--/ Footer Wrapper -->
  </section>
  <!--/ Main Content Wrapper -->
  <!-- Right Sidebar -->
  <aside id="app_sidebar-right">
    <div class="sidebar-inner sidebar-overlay">
      <div class="tabpanel">
        <ul class="nav nav-tabs nav-justified">
          <li class="active" role="presentation"><a href="#sidebar_chat" data-toggle="tab" aria-expanded="true">Tab One</a></li>
          <li role="presentation"><a href="#sidebar_activity" data-toggle="tab">Tab Two</a></li>
          <li role="presentation"><a href="#sidebar_settings" data-toggle="tab">Tab Three</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade active in" id="sidebar_chat">
          </div>
          <div class="tab-pane fade" id="sidebar_activity">
          </div>
          <div class="tab-pane fade" id="sidebar_settings">
          </div>
        </div>
      </div>
    </div>
</aside>
<!--/ Right Sidebar -->
</div>
@include('partials.assets.javascript')
@stack('javascript')
</body>
</html>
