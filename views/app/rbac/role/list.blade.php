@extends('app.container')

@section('content')
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Name </th>
            <th>Slug </th>
            <th>Description </th>
            <th>Granted Access</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          @forelse($roles as $key => $role)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>{{ $role->name }}</td>
              <td>{{ $role->slug }}</td>
              <td>{{ $role->description }}</td>
              <td>{{ $role->permissions->implode('name', ', ') }}</td>
              <td></td>
            </tr>
           @empty
            <tr>
              <td colspan="5" style="text-align: center"><b>Tidak ada data</b></td>
            </tr>
          @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
